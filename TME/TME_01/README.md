# TME_01 Introduction

## Installer ZedBoard

Copier le dossier [zedboard.zip](./zedboard.zip) dans le repertoire de Xilinx. Pour Windows : C:\Xilinx\Vivado\2020.1\data\boards\board_parts\zynq\zed

## Quick Setup

1. Launch Vivado
2. Create new project
3. Name the project. Make sure that the option Create project subdirectory is selected.
4. RTL Project. Do not specify sources at this time is not selected
5. VHDL as Target Language. Mixed as the Simulator Language
6. Next
7. Next
8. Next
9. Select boards from the menu. Click Zedboard from the vendor digilentinc.com
10. Create Block Design and enter the name
11. Add IP -> ZYNQ7 Processing System
12. Run Block automation -> Apply Board Preset
13. Add IP -> AXI GPIO
14. Run Connection Automation -> S_AXI
15. Run Connection Automation -> GPIO
16. Save Block Design
17. Validate Design
18. Create HDL Wrapper by clicking in Sources -> Design Sources
19. Generate Bitstream
20. Open Implemeted Design
21. File -> Export -> Export Hardware. Include bitstream
