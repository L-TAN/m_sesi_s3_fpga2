# Projet FPGA2 : Réseau de neurones de reconnaissance de caractères sur ZedBoard

## Transformation de BMP à binaire

[Image en Binaire 0 1](https://www.dcode.fr/image-binaire)

## Compilation for PC

```bash
make all
```

## Compilation for MCU

### Add math library for SDK

The math "m" option needs to be specified in the Libraries in the C/C++ Build Settings.

1. Right-click in the project > Properties  
2. C/C++ Build > Settings > Tool Settings > ARM v7 gcc linker > Libraries
3. Add...
4. Fill with "m"
5. OK
6. OK

![SDK image](./math_options_201211160312449257.jpg "sdk image")