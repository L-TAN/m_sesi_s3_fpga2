#ifndef _PRINT_H_
#define _PRINT_H_

#include "Constants.h"

#if defined(PC)
#include <stdio.h>

#define PRINT_F(fmt, args...) printf(fmt, ## args)

#ifdef DEBUG
#define DEBUG_PRINT(fmt, args...)    printf(fmt, ## args)
#else
#define DEBUG_PRINT(fmt, args...)    /* Don't do anything in release builds */
#endif

#elif defined(MCU)
#include <xil_printf.h>

#define PRINT_F(fmt, args...) xil_printf(fmt, ## args)

#ifdef DEBUG
#define DEBUG_PRINT(fmt, args...)    xil_printf(fmt, ## args)
#else
#define DEBUG_PRINT(fmt, args...)    /* Don't do anything in release builds */
#endif

#else
#endif

#endif //! _PRINT_H_