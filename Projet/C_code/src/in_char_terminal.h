#ifndef _IN_CHAR_TERMINAL_H_
#define _IN_CHAR_TERMINAL_H_

#include <string.h>

#include "Constants.h"
#include "Print.h"

#ifdef PC
void get_input_char(float *input, size_t sz) {
  char in_val[sz + IMGWIDTH * 2];
  char c;
  size_t i = 0;
  strcpy(in_val, "");
  PRINT_F("Please enter the character file: \r\n");
  while (1) {
    c = getchar();
    strncat(in_val, &c, 1);
    if (c == '1') {
      input[i++] = 1.0;
    } else if (c == '0') {
      input[i++] = 0.0;
    }
    if (i >= sz) {
      break;
    }
  }
  PRINT_F("\r\n");
  PRINT_F("You entered: \r\n");
  PRINT_F("%s\r\n", in_val);
}
#else
#include "xuartps.h"
#define UART_BASEADDR XPAR_PS7_UART_1_BASEADDR

void get_input_char(float *input, size_t sz) {
  char in_val[sz + IMGWIDTH * 2];
  char c;
  u32 CntrlRegister;
  size_t i = 0;
  strcpy(in_val, "");
  CntrlRegister = XUartPs_ReadReg(UART_BASEADDR, XUARTPS_CR_OFFSET);
	XUartPs_WriteReg(UART_BASEADDR, XUARTPS_CR_OFFSET,
				  ((CntrlRegister & ~XUARTPS_CR_EN_DIS_MASK) |
				   XUARTPS_CR_TX_EN | XUARTPS_CR_RX_EN));
  PRINT_F("Please enter the character file: \r\n");
  while (1) {
    // Wait for input from UART via the terminal
	  while (!XUartPs_IsReceiveData(UART_BASEADDR));
		c = XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET);
    strncat(in_val, &c, 1);
    if (c == '1') {
      input[i++] = 1.0;
    } else if (c == '0') {
      input[i++] = 0.0;
    }
    if (i >= sz) {
      break;
    }
  }
  PRINT_F("\r\n");
}
#endif
#endif //! _IN_CHAR_TERMINAL_H_
