/* Personal Headers */
#include "Constants.h"
#include "Print.h"
#include "Time_Count.h"
#include "calc_layers.h"
#include "float_weight.h"
#include "functions_ES.h"
#include "in_char_terminal.h"

#ifdef CREATE_LAYER_H_FILE
#include "print_float_weight.h"
#endif // !CREATE_LAYER_H_FILE

#ifdef MCU
#include "leds.h"
#endif // !MCU

/* STD headers */
#include <float.h>
#include <stddef.h>

//----------------------------------------------------
// MAIN FUNCTION
//----------------------------------------------------
int main(void) {
  // Variables for counting clock time
  TIME_T start_clock, end_clock;
  // Variables for counting clock
  uint32_t InitHostMem_ck;
  uint32_t CalculateLayer1_ck;
  uint32_t CalculateLayer2_ck;
  uint32_t CalculateLayer3_ck;
  uint32_t CalculateLayer4_ck;
  uint32_t CalculateLayer5_ck;
  // Variables for counting time
  double InitHostMem_time;
  double CalculateLayer1_time;
  double CalculateLayer2_time;
  double CalculateLayer3_time;
  double CalculateLayer4_time;
  double CalculateLayer5_time;
  // Threshold to find the number match
  double score_max;
  // Save the final index that match with the number found
  int index_max;

  /* Layers Neurons*/
  float Layer1_Neurons_CPU[LAYER1_NEURON_SZ] = {0.0};
  float Layer2_Neurons_CPU[LAYER2_NEURON_SZ] = {0.0};
  float Layer3_Neurons_CPU[LAYER3_NEURON_SZ] = {0.0};
  float Layer4_Neurons_CPU[LAYER4_NEURON_SZ] = {0.0};
  double Layer5_Neurons_CPU[LAYER5_NEURON_SZ] = {0.0};

  PRINT_F("Initiating program...\r\n");
  PRINT_F("\r\n");
#if defined(INFINITE_LOOP)
  while(1) {
#elif defined(ITERATION_LOOP)
  for (size_t i = 0; i < EXECUTION_TIMES; ++i){
    PRINT_F("####################### Iteration = %lu\r\n\r\n", i+1);
#endif
#ifdef USER_INPUT
    get_input_char(_CHAR_INPUT, LAYER1_NEURON_SZ);
#endif
    // Init Threshold to find the number match
    score_max = FLT_MIN;
    // Init final index that match with the number found
    index_max = ERROR_VAL;
    DEBUG_PRINT("InitHostMem\r\n");
    // 1. Init chronometer
    GET_CLOCK(start_clock);
    // 2. Do the function : Initialize the layers with the values of the files
    InitHostMem(Layer1_Weights_CPU, Layer2_Weights_CPU, Layer3_Weights_CPU,
                Layer4_Weights_CPU);
    // 3. Stop the chronometer
    GET_CLOCK(end_clock);
    // 4. Calculate and print times ****************************************
    InitHostMem_ck = GET_CLOCK_CYCLES(start_clock, end_clock);
    InitHostMem_time = GET_TIME(start_clock, end_clock);

    /*********************** Calculate each layer using the input value (0-9) */
    DEBUG_PRINT("CalculateLayer1\r\n");
    // 1. Init chronometer
    GET_CLOCK(start_clock);
    // 2. Do the function : Calculate Layer 1
    calculateLayer1(_CHAR_INPUT, Layer1_Neurons_CPU);
    // 3. Stop the chronometer
    GET_CLOCK(end_clock);
    // 4. Calculate and print times *******************************************
    CalculateLayer1_ck = GET_CLOCK_CYCLES(start_clock, end_clock);
    CalculateLayer1_time = GET_TIME(start_clock, end_clock);

    DEBUG_PRINT("CalculateLayer2\r\n");
    GET_CLOCK(start_clock);
    calculateLayer2(Layer1_Neurons_CPU, Layer1_Weights_CPU, Layer2_Neurons_CPU);
    GET_CLOCK(end_clock);
    CalculateLayer2_ck = GET_CLOCK_CYCLES(start_clock, end_clock);
    CalculateLayer2_time = GET_TIME(start_clock, end_clock);

    DEBUG_PRINT("CalculateLayer3\r\n");
    GET_CLOCK(start_clock);
    calculateLayer3(Layer2_Neurons_CPU, Layer2_Weights_CPU, Layer3_Neurons_CPU);
    GET_CLOCK(end_clock);
    CalculateLayer3_ck = GET_CLOCK_CYCLES(start_clock, end_clock);
    CalculateLayer3_time = GET_TIME(start_clock, end_clock);

    DEBUG_PRINT("CalculateLayer4\r\n");
    GET_CLOCK(start_clock);
    calculateLayer4(Layer3_Neurons_CPU, Layer3_Weights_CPU, Layer4_Neurons_CPU);
    GET_CLOCK(end_clock);
    CalculateLayer4_ck = GET_CLOCK_CYCLES(start_clock, end_clock);
    CalculateLayer4_time = GET_TIME(start_clock, end_clock);

    DEBUG_PRINT("CalculateLayer5\r\n");
    GET_CLOCK(start_clock);
    calculateLayer5(Layer4_Neurons_CPU, Layer4_Weights_CPU, Layer5_Neurons_CPU);
    GET_CLOCK(end_clock);
    CalculateLayer5_ck = GET_CLOCK_CYCLES(start_clock, end_clock);
    CalculateLayer5_time = GET_TIME(start_clock, end_clock);
    DEBUG_PRINT("\r\n");

    for (uint8_t i = 0; i < LAYER5_NEURON_SZ; ++i) {
      // Print the value of the each neuron of the layer 5
      DEBUG_PRINT("%d : %f\r\n", (int)i, Layer5_Neurons_CPU[i]);
      // If value is greater than the score max, then we found the number
      if (Layer5_Neurons_CPU[i] > score_max) {
        // Save the value of the neuron
        score_max = Layer5_Neurons_CPU[i];
        // Save the index as number
        index_max = i;
      }
    }
    // Error
    if (index_max < 0) {
      PRINT_F("[Error] Finding number \r\n");
    } else {
      // Print the result in the terminal
      PRINT_F("Le resultat est : %d \r\n", index_max);
      PRINT_F("\r\n");
      DEBUG_PRINT("InitHostMem Clock cyles = %u\r\n", InitHostMem_ck);
      DEBUG_PRINT("CalculateLayer1 Clock cyles = %u\r\n", CalculateLayer1_ck);
      DEBUG_PRINT("CalculateLayer2 Clock cyles = %u\r\n", CalculateLayer2_ck);
      DEBUG_PRINT("CalculateLayer3 Clock cyles = %u\r\n", CalculateLayer3_ck);
      DEBUG_PRINT("CalculateLayer4 Clock cyles = %u\r\n", CalculateLayer4_ck);
      DEBUG_PRINT("CalculateLayer5 Clock cyles = %u\r\n", CalculateLayer5_ck);
      DEBUG_PRINT("\r\n");
      DEBUG_PRINT("InitHostMem     Time in sec = %f\r\n", InitHostMem_time);
      DEBUG_PRINT("CalculateLayer1 Time in sec = %f\r\n", CalculateLayer1_time);
      DEBUG_PRINT("CalculateLayer2 Time in sec = %f\r\n", CalculateLayer2_time);
      DEBUG_PRINT("CalculateLayer3 Time in sec = %f\r\n", CalculateLayer3_time);
      DEBUG_PRINT("CalculateLayer4 Time in sec = %f\r\n", CalculateLayer4_time);
      DEBUG_PRINT("CalculateLayer5 Time in sec = %f\r\n", CalculateLayer5_time);
      PRINT_F("\r\n");
#ifdef MCU
      if (display_number_leds(index_max) != 0) {
        PRINT_F("Error displaying value in leds\r\n");
      }
#endif
    }
#ifdef CREATE_LAYER_H_FILE
    print_float_weight();
#endif
  }
  return 0;
}
