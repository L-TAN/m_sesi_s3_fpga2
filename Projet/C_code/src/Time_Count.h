#ifndef _TIME_COUNT_H_
#define _TIME_COUNT_H_

#include "Constants.h"
#include "Print.h"

#if defined(PC)
#include <time.h>

#define TIME_T clock_t
#define GET_CLOCK(clk) clk = clock()
#define GET_CLOCK_CYCLES(start, end) (end - start)
#define GET_TIME(start, end) (((double)(end - start)) / CLOCKS_PER_SEC)
#define PRINT_TIME_CPU_CYCLES(start, end)                                      \
  PRINT_F("Clock cycles = %ld\r\n", (end - start))
#define PRINT_TIME_SEC(start, end)                                             \
  PRINT_F("Time = %f sec\r\n\r\n", (((double)(end - start)) / CLOCKS_PER_SEC))

#elif defined(MCU)
#include <xtime_l.h>

#define TIME_T XTime
#define GET_CLOCK(clk) XTime_GetTime(&clk)
#define GET_CLOCK_CYCLES(start, end) (2 * (end - start))
#define GET_TIME(start, end) (((double)(end - start)) / COUNTS_PER_SECOND)
#define PRINT_TIME_CPU_CYCLES(start, end)                                      \
  PRINT_F("Clock cycles = %llu\r\n", 2 * (end - start))
#define PRINT_TIME_SEC(start, end)                                             \
  PRINT_F("Time = %f sec\r\n\r\n",                                             \
         (((double)(end - start)) / COUNTS_PER_SECOND))

#else
#endif

#endif //! _TIME_COUNT_H_