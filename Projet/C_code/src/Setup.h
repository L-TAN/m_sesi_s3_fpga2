#ifndef _SETUP_H_
#define _SETUP_H_

/************************************************/
/*********** Select number to analyze ***********/
/************************************************/
// #define INPUT_00                      // []
// #define INPUT_01                      // []
// #define INPUT_02                      // []
// #define INPUT_03                      // []
// #define INPUT_04                      // []
// #define INPUT_05                      // []
// #define INPUT_06                      // []
// #define INPUT_07                      // []
// #define INPUT_08                      // []
// #define INPUT_09                      // []
#define USER_INPUT                    // [X]

/**********************************************/
/*********** Select an architecture ***********/
/**********************************************/
// #define PC                            // []
#define MCU                           // [X]

/***************************************/
/*********** Select a Target ***********/
/***************************************/
#define DEBUG                         // [X]
// #define RELEASE                       // []

/***************************************************/
/*********** Select if create layer file ***********/
/***************************************************/
#ifdef PC
// #define CREATE_LAYER_H_FILE           // []
#undef CREATE_LAYER_H_FILE            // [X]
#else
#undef CREATE_LAYER_H_FILE
#endif

/**************************************************************/
/*********** Select infinite loop or iteration loop ***********/
/**************************************************************/
#define INFINITE_LOOP                 // [X]
// #define ITERATION_LOOP                // []

#ifdef ITERATION_LOOP
#define EXECUTION_TIMES (10)
#endif

#endif //! _SETUP_H_
