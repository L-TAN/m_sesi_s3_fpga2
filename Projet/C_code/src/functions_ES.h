#ifndef _FUNCTION_ES_H_
#define _FUNCTION_ES_H_

void InitHostMem(float *Layer1_Weights_CPU, float *Layer2_Weights_CPU,
                 float *Layer3_Weights_CPU, float *Layer4_Weights_CPU);

void readIn(float *layer1);

void output(double *final);

#endif //! _FUNCTION_ES_H_