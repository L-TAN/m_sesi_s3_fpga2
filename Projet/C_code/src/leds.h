#ifndef _LEDS_H_
#define _LEDS_H_

/* STD Include Files */
#include <stdint.h>
#include <stdbool.h>

/* Xilinx Include Files */
#include "xparameters.h"
#include "xgpio.h"
#include "xstatus.h"

/* Definitions */
#define GPIO_DEVICE_ID XPAR_AXI_GPIO_0_DEVICE_ID    /* GPIO device that LEDs are connected to */
#define LED_CHANNEL 1                               /* GPIO port for LEDs */
XGpio Gpio;                                         /* GPIO Device driver instance */
bool is_gpio_init = false;

int init_gpio_leds() {
  /* GPIO driver initialisation */
  int status = XGpio_Initialize( & Gpio, GPIO_DEVICE_ID);
  if (status != XST_SUCCESS) {
    return XST_FAILURE;
  }
  is_gpio_init = true;
  /*Set the direction for the LEDs to output. */
  XGpio_SetDataDirection( & Gpio, LED_CHANNEL, 0x00);
  return status;
}

int display_number_leds(uint8_t val_7_bit) {
  int status = 0;
  if (!is_gpio_init) {
    status = init_gpio_leds();
    if (status != XST_SUCCESS) {
      return XST_FAILURE;
    }
  }
  /* Write output to the LEDs. */
  XGpio_DiscreteWrite( & Gpio, LED_CHANNEL, val_7_bit);
  return XST_SUCCESS;
}

#endif //! _LEDS_H_