#ifndef _PRINT_FLOAT_WEIGHT_H_
#define _PRINT_FLOAT_WEIGHT_H_

#include "Constants.h"

#ifdef CREATE_LAYER_H_FILE
#include <stdio.h>
#include "float_weight.h"

void print_float_weight(void) {
  FILE *fp;
  fp = fopen("float_weight.h", "wb+");
  fprintf(fp, "#ifndef _FLOAT_WEIGHT_H_\r\n");
  fprintf(fp, "#define _FLOAT_WEIGHT_H_\r\n");
  fprintf(fp, "#include \"Constants.h\"\r\n");
  fprintf(fp, "\r\n");
  fprintf(fp, "#if defined(PC)\r\n");
  fprintf(fp, "\r\n");
  fprintf(fp, "float Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ] = {0.0};\r\n");
  fprintf(fp, "float Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ] = {0.0};\r\n");
  fprintf(fp, "float Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ] = {0.0};\r\n");
  fprintf(fp, "float Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ] = {0.0};\r\n");
  fprintf(fp, "\r\n");
  fprintf(fp, "#elif defined(MCU)\r\n");

  fprintf(fp, "float Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ] = {");
  for (size_t i = 0; i < LAYER1_WEIGHTS_SZ; ++i) {
    fprintf(fp, "%f,", Layer1_Weights_CPU[i]);
  }
  fprintf(fp, "};\r\n");

  fprintf(fp, "float Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ] = {");
  for (size_t i = 0; i < LAYER2_WEIGHTS_SZ; ++i) {
    fprintf(fp, "%f,", Layer2_Weights_CPU[i]);
  }
  fprintf(fp, "};\r\n");

  fprintf(fp, "float Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ] = {");
  for (size_t i = 0; i < LAYER3_WEIGHTS_SZ; ++i) {
    fprintf(fp, "%f,", Layer3_Weights_CPU[i]);
  }
  fprintf(fp, "};\r\n");

  fprintf(fp, "float Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ] = {");
  for (size_t i = 0; i < LAYER4_WEIGHTS_SZ; ++i) {
    fprintf(fp, "%f,", Layer4_Weights_CPU[i]);
  }
  fprintf(fp, "};\r\n");

  fprintf(fp, "#endif /* ifdef MCU */\r\n");
  fprintf(fp, "\r\n");
  fprintf(fp, "#endif /* _FLOAT_WEIGHT_H_ */\r\n");

  fclose(fp);
}

#else

void print_float_weight(void){/* DO NOTHING */}

#endif //! CREATE_LAYER_H_FILE

#endif //! _PRINT_FLOAT_WEIGHT_H_