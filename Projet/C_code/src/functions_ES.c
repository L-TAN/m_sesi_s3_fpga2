#include "functions_ES.h"
#include "Setup.h"

#if defined(PC)
#include <stdio.h>
/*
- lw1.wei pour les connexions entre Layer1 et Layer2
- lw2.wei pour les connexions entre Layer2 et Layer3
- lw3.wei pour les connexions entre les couches Layer3 et Layer4
- lw4.wei pour les connexions entre les couches Layer4 et Layer5.
*/
#define FILE_CONN_LAYER_1_AND_LAYER_2 ("lw1.wei")
#define FILE_CONN_LAYER_2_AND_LAYER_3 ("lw2.wei")
#define FILE_CONN_LAYER_3_AND_LAYER_4 ("lw3.wei")
#define FILE_CONN_LAYER_4_AND_LAYER_5 ("lw4.wei")

void InitHostMem(float *Layer1_Weights_CPU, float *Layer2_Weights_CPU,
                 float *Layer3_Weights_CPU, float *Layer4_Weights_CPU) {
  // initial layer 1 weight
  // Open the file for reading. b = This  is strictly  for  compatibility  with
  // C89 and has no effect
  FILE *pFile1 = fopen(FILE_CONN_LAYER_1_AND_LAYER_2, "rb");
  // If error opening the file
  if (pFile1 != NULL) {
    int i;
    for (i = 0; i < 156; ++i)
      fread(&(Layer1_Weights_CPU[i]), sizeof(float), 1, pFile1);
    fclose(pFile1);
  }

  // initial layer 2 weight
  FILE *pFile2 = fopen(FILE_CONN_LAYER_2_AND_LAYER_3, "rb");
  if (pFile2 != NULL) {
    fread(Layer2_Weights_CPU, sizeof(float), 7800, pFile2);
    fclose(pFile2);
  }
  // initial layer 3 weight
  FILE *pFile3 = fopen(FILE_CONN_LAYER_3_AND_LAYER_4, "rb");
  if (pFile3 != NULL) {
    fread(Layer3_Weights_CPU, sizeof(float), 125100, pFile3);
    fclose(pFile3);
  }
  // initial layer 4 weight
  FILE *pFile4 = fopen(FILE_CONN_LAYER_4_AND_LAYER_5, "rb");
  if (pFile4 != NULL) {
    fread(Layer4_Weights_CPU, sizeof(float), 1010, pFile4);
    fclose(pFile4);
  }
}

void readIn(float *layer1) {
  FILE *fp;
  fp = fopen("in.neu", "rb");
  if (fp) {
    fread(layer1, sizeof(float), 29 * 29, fp);
    fclose(fp);
  }
}

void output(double *final) {
  FILE *fp = 0;
  fp = fopen("out.res", "wb");
  if (fp) {
    fwrite(final, sizeof(double), 10, fp);
    fclose(fp);
  }
}


#elif defined(MCU)
void InitHostMem(float *Layer1_Weights_CPU, float *Layer2_Weights_CPU,
                 float *Layer3_Weights_CPU, float *Layer4_Weights_CPU) {
  /* DO NOTHING */
}

void readIn(float *layer1) { /*TODO: code */
}

void output(double *final) { /*TODO: code */
}
#endif